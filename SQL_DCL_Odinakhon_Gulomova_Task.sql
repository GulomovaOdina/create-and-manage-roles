-- Create a new user
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';

-- Grant connect permission to the user
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

-- Grant SELECT permission for the "customer" table to rentaluser
GRANT SELECT ON customer TO rentaluser;

-- Check if SELECT permission works correctly for rentaluser
SELECT * FROM customer;

-- Create a new user group
CREATE GROUP rental;

-- Add rentaluser to the rental group
GRANT rental TO rentaluser;

-- Grant INSERT and UPDATE permissions for the "rental" table to the rental group
GRANT INSERT, UPDATE ON rental TO rental;

-- Insert a new row into the "rental" table under the rental role
INSERT INTO rental (rental_id, rental_date, inventory_id, customer_id, return_date, staff_id, last_update) 
VALUES 
  (1, '2024-03-24 10:00:00', 9, 8, '2024-04-01 10:00:00', 1, CURRENT_TIMESTAMP);



-- Update an existing row in the "rental" table under the rental role
UPDATE rental
SET return_date = '2024-04-05 10:00:00'
WHERE customer_id = 8;


-- Revoke INSERT permission for the "rental" table from the rental group
REVOKE INSERT ON rental FROM rental;



-- Create a personalized role for an existing customer (replace placeholders with actual data)
CREATE ROLE client_Odinakhon_Gulomova;

-- Grant access to own data in the "rental" table
GRANT SELECT, INSERT, UPDATE ON rental TO client_Odinakhon_Gulomova;
ALTER DEFAULT PRIVILEGES IN SCHEMA public FOR ROLE client_Odinakhon_Gulomova GRANT SELECT, INSERT, UPDATE ON TABLES TO client_Odinakhon_Gulomova;

-- Grant access to own data in the "payment" table
GRANT SELECT ON payment TO client_Odinakhon_Gulomova;
ALTER DEFAULT PRIVILEGES IN SCHEMA public FOR ROLE client_Odinakhon_Gulomova GRANT SELECT ON TABLES TO client_Odinakhon_Gulomova;

-- Write a query to make sure this user sees only their own data (replace placeholders with actual data)
SET ROLE client_Odinakhon_Gulomova;
SELECT * FROM rental WHERE customer_id = '7';
SELECT * FROM payment WHERE customer_id = '7';
RESET ROLE;
